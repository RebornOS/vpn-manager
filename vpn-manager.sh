#!/bin/bash
if [[ $LANG = *"en"* ]]; then
export MANAGER="Manager"
export RUNNING="Running"
export NOT_RUNNING="Not Running"
export STATUS="Status"
export ENABLE="Enable"
export DISABLE="Disable"
export MINIMIZE="Minimize to System Tray"
export USERNAME1="Username"
export PASS="Password"
export ROOT_PASS="Root Password"
export LOC="Connection Location"
export ACTIVATION_CODE_TEXT="Please insert your activation code from here,  https://www.expressvpn.com/subscriptions, as well as enter your root password in order to enable $VPN to start upon login automatically."
export ACTIVATION_CODE="Activation Code"
export INFO="Share crash reports, speed tests, and whether VPN connection attempts succeed with"
export CONFIGURE="Configure"
export PROTOCOL="Protocol"
export COUNTRY_CODE1="Two letter Country Code (optional)"
export CHOOSE_VPN="Choose VPN"

elif [[ $LANG = *"es"* ]]; then
export MANAGER="Gerente"
export RUNNING="Corriendo"
export NOT_RUNNING="No Corriendo"
export STATUS="Estado"
export ENABLE="Permitir"
export DISABLE="No Permitir"
export MINIMIZE="Minimizar a la Bandeja del Sistema"
export USERNAME1="Nombre de Usuario"
export PASS="Contraseña"
export ROOT_PASS="Contraseña de Root"
export LOC="Ubicación de la Conexión"
export ACTIVATION_CODE_TEXT="Por favor, introduzca su código de activación desde aquí, https://www.expressvpn.com/subscriptions, así como su contraseña de root para permitir que $VPN se inicie automáticamente al iniciar sesión."
export ACTIVATION_CODE="Código de Activación"
export INFO="Comparta informes de fallos, pruebas de velocidad y si los intentos de conexión VPN han tenido éxito con"
export CONFIGURE="Configurar"
export PROTOCOL="Protocolo"
export COUNTRY_CODE1="Código de país de dos letras (opcional)"
export CHOOSE_VPN="Elige VPN"

elif [[ $LANG = *"fr"* ]]; then
export MANAGER="Directeur"
export RUNNING="Fonctionnement"
export NOT_RUNNING="Ne pas Courrir"
export STATUS="Statut"
export ENABLE="Permettre"
export DISABLE="Désactiver"
export MINIMIZE="Réduire dans la Barre d'état Système"
export USERNAME1="Nom d'utilisateur"
export PASS="Mot de Passe"
export ROOT_PASS="Mot de Passe Root"
export LOC="Lieu de Connexion"
export ACTIVATION_CODE_TEXT="Veuillez insérer votre code d'activation à partir d'ici, https://www.expressvpn.com/subscriptions, ainsi que votre mot de passe root afin de permettre à $VPN de démarrer automatiquement à la connexion."
export ACTIVATION_CODE="Code d'activation"
export INFO="Partagez les rapports de bogues, les tests de vitesse et vérifiez si les tentatives de connexion VPN ont été couronnées de succès avec"
export CONFIGURE="Configurer"
export PROTOCOL="Protocole"
export COUNTRY_CODE1="Code de pays à deux lettres (facultatif)"
export CHOOSE_VPN="Choisissez un VPN"

elif [[ $LANG = *"hi"* ]]; then
export MANAGER="मैनेजर"
export RUNNING="रनिंग"
export NOT_RUNNING="चल नहीं रहा"
export STATUS="स्थिति"
export ENABLE="सक्षम करें"
export DISABLE="लंगड़ा बनाना"
export MINIMIZE="प्रणाली थाली में कम करें"
export USERNAME1="उपयोगकर्ता नाम"
export PASS="पारण शब्द"
export ROOT_PASS="रूट पासवर्ड"
export LOC="कनेक्शन स्थान"
export ACTIVATION_CODE_TEXT="कृपया यहां से अपना सक्रियण कोड डालें, https://www.expressvpn.com/subscriptions, साथ ही स्वचालित रूप से लॉगिन पर शुरू करने के लिए $ VPN को सक्षम करने के लिए अपना रूट पासवर्ड दर्ज करें।"
export ACTIVATION_CODE="एक्टिवेशन कोड"
export INFO="क्रैश रिपोर्ट, स्पीड टेस्ट, और वीपीएन कनेक्शन प्रयासों के साथ सफल होने का प्रयास करें"
export CONFIGURE="कॉन्फ़िगर"
export PROTOCOL="मसविदा बनाना"
export COUNTRY_CODE1="दो पत्र देश कोड (वैकल्पिक)"
export CHOOSE_VPN="वीपीएन चुनें"

elif [[ $LANG = *"ar"* ]]; then
export MANAGER="مدير"
export RUNNING="جري"
export NOT_RUNNING="لا يعمل"
export STATUS="حالة"
export ENABLE="مكن"
export DISABLE="تعطيل"
export MINIMIZE="يخفض الى قاع النظام"
export USERNAME1="اسم المستخدم"
export PASS="كلمه السر"
export ROOT_PASS="كلمة المرور الجذر"
export LOC="موقع الاتصال"
export ACTIVATION_CODE_TEXT="الرجاء إدخال رمز التنشيط الخاص بك من هنا ، https://www.expressvpn.com/subscriptions ، بالإضافة إلى إدخال كلمة مرور الجذر من أجل تمكين $ VPN للبدء عند تسجيل الدخول تلقائيًا."
export ACTIVATION_CODE="رمز التفعيل"
export INFO="مشاركة تقارير الأعطال واختبارات السرعة وما إذا كانت محاولات اتصال VPN ناجحة أم لا"
export CONFIGURE="تهيئة"
export PROTOCOL="بروتوكول"
export COUNTRY_CODE1="الرقم الدولي"
export CHOOSE_VPN="VPN اختر "

elif [[ $LANG = *"pt"* ]]; then
export MANAGER="Gerente"
export RUNNING="Corrida"
export NOT_RUNNING="Não Correndo"
export STATUS="Estado"
export ENABLE="Permitir"
export DISABLE="Desativar"
export MINIMIZE="Minimizar a Bandeja do Sistema"
export USERNAME1="Nome de Usuário"
export PASS="Palavra de Passe"
export ROOT_PASS="Palavra de Passe Raiz"
export LOC="Localização da Conexão"
export ACTIVATION_CODE_TEXT="Por favor, insira o seu código de ativação aqui, https://www.expressvpn.com/subscriptions, assim como insira sua senha de root para habilitar $ VPN para iniciar no login automaticamente."
export ACTIVATION_CODE="Código de Ativação"
export INFO="Compartilhar relatórios de falhas, testes de velocidade e se as tentativas de conexão VPN são bem-sucedidas"
export CONFIGURE="Configurar"
export PROTOCOL="Protocolo"
export COUNTRY_CODE1="Código do País"
export CHOOSE_VPN="Escolha VPN"

elif [[ $LANG = *"de"* ]]; then
export MANAGER="Manager"
export RUNNING="Laufen"
export NOT_RUNNING="Nicht Laufen"
export STATUS="Status"
export ENABLE="Ermächtigen"
export DISABLE="Deaktivieren"
export MINIMIZE="Minimieren Sie in der Taskleiste"
export USERNAME1="Nutzername"
export PASS="Passwort"
export ROOT_PASS="Root-Passwort"
export LOC="Verbindungsort"
export ACTIVATION_CODE_TEXT="Bitte geben Sie Ihren Aktivierungscode von hier, https://www.expressvpn.com/subscriptions, sowie Ihr Root-Passwort ein, damit $VPN beim Login automatisch starten kann."
export ACTIVATION_CODE="Aktivierungscode"
export INFO="Teilen Sie Absturzberichte, Geschwindigkeitstests und ob Versuche mit VPN-Verbindungen erfolgreich sind."
export CONFIGURE="Konfigurieren"
export PROTOCOL="Protokoll"
export COUNTRY_CODE1="Landesvorwahl"
export CHOOSE_VPN="Wählen Sie VPN aus"

elif [[ $LANG = *"it"* ]]; then
export MANAGER="Gestore"
export RUNNING="In esecuzione"
export NOT_RUNNING="Non in esecuzione"
export STATUS="Stato"
export ENABLE="Abilita"
export DISABLE="Disabilita"
export MINIMIZE="Minimizza nel tray di sistema"
export USERNAME1="Nome utente"
export PASS="Password"
export ROOT_PASS="Password di root"
export LOC="Locazione della connessione"
export ACTIVATION_CODE_TEXT="Si prega di inserire il proprio codice di attivazione ottenuto da https://www.expressvpn.com/subscriptions, oltre ad inserire la propria password di root al fine di abilitare l'avvio automatico al login di $VPN."
export ACTIVATION_CODE="Codice di attivazione"
export INFO="Condividi i rapporti di crash, test della velocità e se il collegamento alla VPN è riuscito con"
export CONFIGURE="Configura"
export PROTOCOL="Protocollo"
export COUNTRY_CODE1="Codice dello Stato di due lettere (opzionale)"
export CHOOSE_VPN="Scegli la VPN"

elif [[ $LANG = *"zh"* ]]; then
export MANAGER="经理"
export RUNNING="累"
export NOT_RUNNING="没跑"
export STATUS="状态"
export ENABLE="启用"
export DISABLE="禁用"
export MINIMIZE="最小化到系统托盘"
export USERNAME1="用户名"
export PASS="密码"
export ROOT_PASS="Root密码"
export LOC="连接位置"
export ACTIVATION_CODE_TEXT="请从此处插入您的激活码，https：//www.expressvpn.com/subscriptions，并输入您的root密码，以便在登录时自动启动$ VPN。"
export ACTIVATION_CODE="激活码"
export INFO="共享崩溃报告，速度测试以及VPN连接尝试是否成功"
export CONFIGURE="配置"
export PROTOCOL="协议"
export COUNTRY_CODE1="国家代码"
export CHOOSE_VPN="选择VPN"

else
export MANAGER="Manager"
export RUNNING="Running"
export NOT_RUNNING="Not Running"
export STATUS="Status"
export ENABLE="Enable"
export DISABLE="Disable"
export MINIMIZE="Minimize to System Tray"
export USERNAME1="Username"
export PASS="Password"
export ROOT_PASS="Root Password"
export LOC="Connection Location"
export ACTIVATION_CODE_TEXT="Please insert your activation code from here,  https://www.expressvpn.com/subscriptions, as well as enter your root password in order to enable $VPN to start upon login automatically."
export ACTIVATION_CODE="Activation Code"
export INFO="Share crash reports, speed tests, and whether VPN connection attempts succeed with"
export CONFIGURE="Configure"
export PROTOCOL="Protocol"
export COUNTRY_CODE1="Two letter Country Code (optional)"
export CHOOSE_VPN="Choose VPN"
fi

STATUS=$RUNNING

KILL(){
export PID=$(pidof yad)
export MYPPID=$(ps -o ppid= $PID)
}

Stop()
{
	if [[ $VPN = Windscribe ]]; then
		windscribe logout
	elif [[ $VPN = ExpressVPN ]]; then
		expressvpn disconnect

	elif [[ $VPN = NordVPN ]]; then
		nordvpn disconnect

	elif [[ $VPN = PIA ]]; then
		killall -q pia && killall -q pia
		killall -q openvpn && killall -q openvpn
	fi
	pkill yad
	export STATUS="$NOT_RUNNING"
	CONTROL
}

MINIMIZE(){
pkill yad
yad --notification \
    --image=/usr/share/icons/windscribe-reborn.svg \
    --text="$STATUS" \
    --command='bash -c "CONTROL"'
}

CONTROL()
{
KILL
    yad --form \
        --title="$VPN" \
        --width=400 \
	--window-icon=/usr/share/icons/windscribe-reborn.svg \
        --field="$STATUS":RO    "$STATUS" \
        --field="$ENABLE":fbtn          '@bash -c "CONFIGURE"' \
        --field="$DISABLE":fbtn           '@bash -c "Stop"' \
       --field="$MINIMIZE":fbtn         '@bash -c "MINIMIZE"' \
        --button=gtk-quit:"pkill yad"

}

CONFIGURE(){

if [[ $VPN = Windscribe ]]; then
	if [[ -f /usr/bin/windscribe ]]; then
	creds=$(yad --center \
                        --width=350 \
                        --height=100 \
			--window-icon=/usr/share/icons/windscribe-reborn.svg \
                        --form \
                        --title="Login" \
                        --save \
                        --field="$VPN $USERNAME1" \
                        --field="$VPN $PASS":H)
	echo
	echo
#	export creds=${creds%|*}
#	export USERNAME=${creds%|*}
#	export PASSWORD=${creds#*|}

	# set variables
	set password $PASSWORD
	set username $USERNAME

	# get into expect environment to login for the user automatically
	expect -c "
	spawn windscribe login
	expect \"Windscribe Username:\"
	send -- \"$USERNAME\r\"
	expect \"Windscribe Password:\"
	send -- \"$PASSWORD\r\"
	expect eof
	"
	#windscribe-login.sh
	windscribe connect

	#Get ready to launch the CONTROL function
	pkill yad
	export STATUS="$RUNNING"
	CONTROL
	else
		xterm -e yay -S windscribe-cli --noconfirm
		CONFIGURE
	fi

elif [[ $VPN = PIA ]]; then
	if [[ -f /etc/private-internet-access/login.conf ]]; then
		export STATUS="$RUNNING"
		creds=$(yad --center \
                        --width=350 \
                        --height=100 \
			--window-icon=/usr/share/icons/windscribe-reborn.svg \
                        --form \
                        --title="Login" \
                        --save \
			--field="$ROOT_PASS":H \
                        --field="$VPN $USERNAME1" \
                        --field="$VPN $PASS":H)
		echo
		echo
		export creds1=${creds%|*}
		export PASSWORD=${creds1##*|}
		export creds2=${creds1#*|}
		export USERNAME=${creds2%|*}
		export ROOT=${creds1%%|*}

#		echo
#		echo "Password = $PASSWORD"
#		echo "Username = $USERNAME"
#		echo "Root = $ROOT"
#		echo

		echo $ROOT | sudo ls /etc/openvpn/client/ | grep .conf >/home/$USER/pial.txt
		echo $ROOT | sudo chmod 777 /home/$USER/pial.txt
		export LINES=$(wc -l < /home/$USER/pial.txt)
		export LOCATIONS=""
		number=0
		while [  $number -lt $LINES ]; do
			let number=number+1
			echo "Running through loop for time number $number"
			SAVED=$(sed "$number q;d" /home/$USER/pial.txt)
			export LOCATIONS="$LOCATIONS!$SAVED"
		done
		choose=$(yad --center \
				        --width=350 \
				        --height=100 \
					--window-icon=/usr/share/icons/windscribe-reborn.svg \
				        --form \
				        --title="$VPN" \
				        --save \
				        --field="$VPN $LOC:CB" "^$LOCATIONS")
		export LOCATION=${choose%|*}
		echo $ROOT | sudo rm -f /home/$USER/pial.txt
		echo $ROOT | sudo openvpn --config /etc/openvpn/client/$LOCATION & CONTROL
	else
		xterm -e yay -S private-internet-access-vpn --noconfirm
		CONFIGURE
	fi

elif [[ $VPN = ExpressVPN ]]; then
	if [[ -f /usr/bin/expressvpn ]]; then
		expressvpn list >/home/$USER/location-list.txt
		SAVING=$(awk '{print $1}' /home/$USER/location-list.txt)
		$SAVING >/home/$USER/locations.txt
		export LINES=$(wc -l < /home/$USER/locations.txt)
		export LOCATIONS=""
		number=2
		while [  $number -lt $LINES ]; do
			let number=number+1
			echo "Running through loop for time number $number"
			SAVED=$(sed "$number q;d" /home/$USER/locations.txt)
			export LOCATIONS="$LOCATIONS!$SAVED"
		done
		choose=$(yad --center \
				        --width=350 \
				        --height=100 \
					--window-icon=/usr/share/icons/windscribe-reborn.svg \
				        --form \
				        --title="$VPN" \
				        --save \
				        --field="$VPN $LOC:CB" "^$LOCATIONS")
		export LOCATION=${choose%|*}
		expressvpn connect $LOCATION
		rm -f /home/$USER/locations.txt
		rm -f /home/$USER/location-list.txt
		CONTROL
	else
		xterm -e yay -S expressvpn --noconfirm --needed
		activate=$(yad --center \
                        --width=350 \
                        --height=100 \
			--window-icon=/usr/share/icons/windscribe-reborn.svg \
                        --form \
			--text-align=center \
			--selectable-labels \
                        --title="Activate" \
                        --save \
			--text="$ACTIVATION_CODE_TEXT" \
			--field="$ACTIVATION_CODE":H \
			--field="$ROOT_PASS":H \
			--field="$INFO $VPN":CHK \
			--field="$CONFIGURE $LOC":CHK)	

		export activate1=${activate%|*}
		export ADVANCED=${activate1##*|}
		export activate2=${activate1#*|}
		export activate3=${activate2%|*}
		export ROOT=${activate3%|*}
		export OPT_IN=${activate3#*|}
		export ACTIVATE=${activate%%|*}

#echo
#echo
#echo "Root = $ROOT"
#echo "OPT_IN = $OPT_IN"
#echo "Activation key = $ACTIVATE"
#echo "Adcanced = $ADVANCED"
		if [[ $OPT_IN = TRUE ]]; then
			export OPT=Y
		else
			export OPT=n
		fi
		echo $ROOT | sudo systemctl start expressvpn.service
		echo $ROOT | sudo systemctl enable expressvpn.service

		# set variables
		set activate $ACTIVATE

		# get into expect environment to activate the VPN automatically
		expect -c "
		spawn expressvpn activate
		expect \"Enter Activation Code:\"
		send -- \"$ACTIVATE\r\"
		expect \"Help improve ExpressVPN: Share crash reports, speed tests, and whether VPN connection attempts succeed. These reports never contain personally identifiable information. (Y/n)\"
		send -- \"$OPT\r\"
		expect eof
		"
		CONFIGURE
	fi

elif [[ $VPN = NordVPN ]]; then
	if [[ -f /usr/bin/nordvpn ]]; then
		creds3=$(yad --center \
                        --width=350 \
                        --height=100 \
			--window-icon=/usr/share/icons/windscribe-reborn.svg \
                        --form \
                        --title="Login" \
                        --save \
                        --field="$VPN $PROTOCOL:CB" "^udp!tcp" \
                        --field="$COUNTRY_CODE1" \
			--field="$ROOT_PASS:H" \
                        --field="$VPN $USERNAME1" \
                        --field="$VPN $PASS:H")
		echo
		echo
		export creds4=${creds3%|*}
		export PASSWORD=${creds4##*|}
		export creds5=${creds4#*|}
		export creds6=${creds5%|*}
		export creds7=${creds6%|*}
		export ROOT=${creds7#*|}
		export MYUSERNAME=${creds6##*|}
		export PROTOCOL=${creds4%%|*}
		export CODE=${creds7%|*}

#		echo
#		echo "$creds4"
#		echo "Password = $PASSWORD"
#		echo "Username = $USERNAME"
#		echo "Root = $ROOT"
#		echo "Protocol = $PROTOCOL"
#		echo "Code = $CODE"
#		echo
		pkill yad
		echo $ROOT | sudo nordvpn refresh
		echo $ROOT | sudo bash -c "$FUNC; EXPECTING"
		echo $ROOT | nordvpn set protocol $PROTOCOL
		echo $ROOT | nordvpn connect $CODE
		CONTROL
	else
		xterm -e sudo pacman -S nordvpn-bin --noconfirm
		CONFIGURE
	fi

elif [[ $VPN = AirVPN ]]; then
	if [[ -d /usr/share/eddie-ui ]]; then
		pkill yad
		airvpn
	else
		xterm -e yay -S eddie-ui --noconfirm
		CONFIGURE
	fi
fi
}

EXPECTING() {
		# get into expect environment to login for the user automatically
		expect -c "
		spawn sudo nordvpn login
		expect \"Email / Username:\"
		send -- \"$MYUSERNAME\r\"
		expect \"Password:\"
		send -- \"$PASSWORD\r\"
		expect eof
		"
}

CHOOSE(){
vpn=$(yad --center \
                        --width=350 \
                        --height=100 \
			--window-icon=/usr/share/icons/windscribe-reborn.svg \
                        --form \
                        --title="VPN $MANAGER" \
                        --save \
                        --field="$CHOOSE_VPN:CB" "^Windscribe!PIA!ExpressVPN!NordVPN!AirVPN")
export VPN=${vpn%|*}
echo "The VPN that was chosen was $VPN"
CONFIGURE
}

export FUNC=$(declare -f EXPECTING)
export -f CONTROL CONFIGURE Stop MINIMIZE KILL CHOOSE EXPECTING

CHOOSE
